package com.jamrob.test;

import junit.framework.TestCase;

import com.jamrob.util.Log;

public class LoggerTest extends TestCase {
	protected void setUp() throws Exception {
		super.setUp();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testGetInstance() {
		Log log = Log.getInstance(this.getName());
		assertNotNull(log);
	}
	
	public void testInfo() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.info(LoggerTest.class.getName(), "This is an INFO message");
		log.info(LoggerTest.class.getName(), "This is an INFO message, coming from cachedLogger");
	}
	
	public void testWarn() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.warn(LoggerTest.class.getName(), "This is an WARN message");
	}
	
	public void testError() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.error(LoggerTest.class.getName(), "This is an ERROR message");
	}
	
	public void testDebug() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.debug(LoggerTest.class.getName(), "This is an DEBUG message");
	}

	public void testEntering() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		String[] args = new String[] {"1","2","3"};
		log.enter(LoggerTest.class.getName(),"testEntering()", args);
	}
	
	public void testEnteringWithNullParams() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.enter(LoggerTest.class.getName(),"testEnteringWithNullParams()", null);
	}
	
	public void testExiting() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		String[] args = new String[] {"1","2","3"};
		log.exit(LoggerTest.class.getName(),"testExiting()", args);
	}
	
	public void testExitingWithNullValue() {
		Log log = Log.getInstance(LoggerTest.class.getName());
		assertNotNull(log);
		log.exit(LoggerTest.class.getName(),"testExitingWithNullValue()", null);
	}



}
