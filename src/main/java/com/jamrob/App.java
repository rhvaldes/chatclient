package com.jamrob;

import java.util.logging.Logger;

import com.jamrob.chat.manager.XMPPManager;
import com.jamrob.util.Log;

/**
 * Hello world!
 * 
 */
public class App {
	
	public static void main(String[] args) throws Exception {
		System.out.println("Testing IM client with google talk...");
		String username = "rhvaldes";
		String password = "val128Es";

		String server = "talk.google.com";
		int port = 5222;
		String service = "gmail.com";

		XMPPManager xmppManager = new XMPPManager(server, 5222, service);

		xmppManager.init();
		System.out.println(String.format(
				"Initializing connection to server %1$s port %2$d using %3$s",
				server, port, service));

		xmppManager.performLogin(username, password);
		System.out.println("Login......");

		xmppManager.setStatus(XMPPManager.STATUS_AVAILABLE,
				"Testing my client...");
		System.out.println("Setting status......");

		String buddyJID = "rhvaldes";
		String buddyName = "rhvaldes";
		xmppManager.createEntry(buddyJID, buddyName);
		System.out.println("Creating an entry to test against....");

		xmppManager.sendMessage("Testing 1 2 3", "rhvaldes");
		System.out.println("Sending message....");

		boolean isRunning = true;

		while (isRunning) {
			Thread.sleep(5);
		}

		xmppManager.destroy();
		System.out.println("ENDING");

	}
}
