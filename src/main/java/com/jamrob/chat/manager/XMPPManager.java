package com.jamrob.chat.manager;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.packet.Presence.Type;

import com.jamrob.chat.listener.ChatMessageListener;

public class XMPPManager {
	public static final Type STATUS_AVAILABLE = Type.available;
	public static final Type STATUS_UNAVAILABLE = Type.unavailable;

	private static final int packetReplyTimeout = 500;
	private String server;
	private String serviceName;
	private int port;

	private ConnectionConfiguration config;
	private XMPPConnection connection;

	private ChatManager chatManager;
	private MessageListener messageListener;

	public XMPPManager(String server, int port, String serviceName) {
		this.server = server;
		this.port = port;
		this.serviceName = serviceName;
	}

	public void init() throws XMPPException {
		SmackConfiguration.setPacketReplyTimeout(packetReplyTimeout);
		config = new ConnectionConfiguration(server, port, serviceName);
		config.setSASLAuthenticationEnabled(false);
		config.setSecurityMode(SecurityMode.enabled);
		connection = new XMPPConnection(config);
		connection.connect();
		chatManager = connection.getChatManager();
		messageListener = new ChatMessageListener();
	}

	public void performLogin(String username, String password)
			throws XMPPException {
		if (connection != null && connection.isConnected()) {
			connection.login(username, password);
		}
	}

	public void setStatus(Type status, String statusMessage) {
		Presence presence = new Presence(status);
		presence.setStatus(statusMessage);
		connection.sendPacket(presence);
	}

	public void destroy() {
		if (connection != null && connection.isConnected()) {
			connection.disconnect();
		}
	}

	public void sendMessage(String message, String buddyJID)
			throws XMPPException {
		Chat chat = chatManager.createChat(buddyJID, messageListener);
		chat.sendMessage(message);
	}

	public void createEntry(String user, String name) throws Exception {
		Roster roster = connection.getRoster();
		roster.createEntry(user, name, null);
	}

}
