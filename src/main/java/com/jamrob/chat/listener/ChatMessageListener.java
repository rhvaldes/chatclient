package com.jamrob.chat.listener;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

public class ChatMessageListener implements MessageListener {
	private String from;
	private String body;
	private String msg;

	public void processMessage(Chat chat, Message message) {
		String from = message.getFrom();
		String body = message.getBody();
		System.out.println(String.format("Received message '%1$s' from %2$s",
				body, from));
	}
}