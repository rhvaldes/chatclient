package com.jamrob.util;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.message.Message;

public class Log {
	private static HashMap<String, Logger> loggers = new HashMap<String, Logger>();
	protected static Log instance = null;
	private static Logger cachedLogger;

	private Log() {
		super();
	}

	public static Log getInstance(String className) {
		if (instance == null) {
			instance = new Log();
			cachedLogger = instance.getLogger(className);
		}
		return instance;
	}

	private Logger getLogger(String className) {
		Logger logger = (Logger) loggers.get(className);
		if (logger == null) {
			logger = LogManager.getLogger(className);
			loggers.put(className, logger);
		}
		return logger;
	}

	public void info(String className, String msg) {
		getCachedLogger(className).info(msg);
	}

	public void warn(String className, String msg) {
		getCachedLogger(className).warn(msg);
	}

	public void error(String className, String msg) {
		getCachedLogger(className).error(msg);
	}

	public void debug(String className, String msg) {
		getCachedLogger(className).debug(msg);
	}

	public void trace(String className, String msg) {
		getCachedLogger(className).trace(msg);
	}

	public void enter(String className, String method, Object[] args) {
		StringBuilder sb = new StringBuilder(method + " - Entering");
		if (null == args) {
			getCachedLogger(className).trace(sb.toString());
		} else {
			getCachedLogger(className)
					.trace(method + " - Entering with  "
							+ entryMsg(args.length, args));
		}
	}

	public void exit(String className, String method, Object[] args) {
		StringBuilder sb = new StringBuilder(method + " - Exiting");
		if (null == args) {
			getCachedLogger(className).trace(sb.toString());
		} else {
			getCachedLogger(className).trace(
					method + " - Exiting with  " + entryMsg(args.length, args));
		}
	}

	private Logger getCachedLogger(String className) {
		if (!className.equals(cachedLogger.getName())) {
			cachedLogger = getLogger(className);
		}
		return cachedLogger;
	}

	protected StringBuilder entryMsg(final int count, final Object... params) {
		final StringBuilder sb = new StringBuilder("params[");
		if (count == 0) {
			return sb.append("]");
		}
		int i = 0;
		for (final Object parm : params) {
			if (parm != null) {
				sb.append(parm.toString());
			} else {
				sb.append("null");
			}
			if (++i < params.length) {
				sb.append(", ");
			}
		}
		sb.append(']');
		return sb;
	}

}
