package com.jamrob.gui.view;

import java.awt.FlowLayout;

import javax.swing.JFrame;

public class ChatClientFrame extends JFrame {
	private static final String FRAME_TITLE = "Chat";
	/**
	 * 
	 */
	private static final long serialVersionUID = 7670778814859280701L;

	public ChatClientFrame() {
		initializeFrame();
	}
	
	private void initializeFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER));
		setTitle(FRAME_TITLE);
		setSize(600, 280);
		setVisible(true);		
		
	}


}
